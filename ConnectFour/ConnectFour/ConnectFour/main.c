//
//  main.c
//  Connect4_AgustinTamayo
//
//  Created by Agustín Tamayo Quiñones on 26/06/2020.
//  Copyright © 2020 Agustín Tamayo Quiñones. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define N 8
#define PROFUNIDAD 6

int calculatePosition(int column, int board[N][N]);
int win(int board[N][N], int column);
int boardFull(int finally, int board[N][N]);
int isColumnFull(int column, int board[N][N]);

int max(int num1, int num2);
int min(int num1, int num2);

int min(int num1, int num2)
{
    return (num1 > num2 ) ? num2 : num1;
}

int max(int num1, int num2)
{
    return (num1 > num2 ) ? num1 : num2;
}


//IA


typedef struct node {
    int board[N][N];
    struct node *children[N];
    int n_children;
    int columnUsed;
    int positionUsed;
    int value;   //heuristic value
    int scooreVertical;
    int scooreHoritzontal;
} Node;


//COPIA TABLERO
void copyBoard(int board[N][N], int parent[N][N]){
   // memcpy (board, parent, sizeof(parent));
    for(int i = 0; i != N; i++){
        for(int j = 0; j != N; j++){
            board[i][j] = parent[i][j];
        }
    }
}


//CUANTOS HIJOS?
int numOfChildren(int board[N][N]){
    int numChild = 0;
    for(int i = 0; i != N; i++){
        if(board[0][i] == 0){
            numChild++;
        }
    }
    return numChild;
}

int knowColumn(int board[N][N], int numChild){
    int column = -1;
    int empty = 0;
    for(int i = 0; i != N; i++){
        if(board[0][i] == 0){
            if(empty == numChild){
                column = i;
                return column;
            }
            empty++;
        }
    }
    return column;
}

//Poner ficha
void applyThrow(int board[N][N], int numChild, int player, int column, int position){
    //saber columna
    //int column = knowColumn(board, numChild);
    //saber fila (calculate position)
    //int position = calculatePosition(column, board);
    //poner ficha
    board[position][column] = player;
}




Node *createNode(Node *parent,int numChild, int level) {
    Node *p=malloc(sizeof(Node));
    copyBoard(p->board,parent->board);  //Student: just copies a matrix onto another
    //level
    if(level%2 == 0){
        p->columnUsed = knowColumn(p->board, numChild);
        p->positionUsed = calculatePosition(p->columnUsed, p->board);
        p->scooreHoritzontal = 0;
        p->scooreVertical = 0;
        applyThrow(p->board,numChild, 1, p->columnUsed, p->positionUsed); //Student: use gravity to find the row from the appropriate column
    } else{
        p->columnUsed = knowColumn(p->board, numChild);
        p->positionUsed = calculatePosition(p->columnUsed, p->board);
        p->scooreVertical = 0;
        p->scooreHoritzontal = 0;
        applyThrow(p->board,numChild, 2, p->columnUsed, p->positionUsed); //Student: use gravity to find the row from the appropriate column
    }
    if (level<PROFUNIDAD) {
        p->n_children=numOfChildren(p->board);  //Student: returns the quantity of free columns on the board
    }
    else {
        p->n_children=0;
    }
    return p;
}

void createChildren(Node *parent,int level) {
    int i;
    for (i=0;i<parent->n_children;i++) {
        parent->children[i]=createNode(parent,i,level);
    }
}

//We cosider root node alreade created and n_children already set.
void createTree(Node *root, int profundidad) {
    int i;
    createChildren(root,profundidad);   //1st generation
    for(i=0;i<root->n_children;i++) {       //creaes the 2nd generation
        //createChildren(root->children[i],2);
        createTree(root->children[i], profundidad+1);
    }
}

int emptyHoritzontal(int board[N][N], int column, int position){
    int empty = 0;
    for(int i = 0; i!=position; i++){
        if(board[i][column] == 0){
            empty++;
        }
    }
    return empty;
}

int posibilitieHoritzontal(int board[N][N], int column, int position, int player){
    
    int score = 10;
    int empty = 0;
    int imIn = 0;
    for(int i = 0; i!=N; i++){
        if(board[position][i] == 0){
            empty++;
            score += 10;
            score -= (emptyHoritzontal(board, i, position) * 2);
        }else if (board[position][i] == player){
            empty++;
            imIn = 1;
            score += 10;
        }else{
            empty = 0;
            imIn = 0;
            score = 10;
        }
        
        if(empty == 4 && imIn == 1){
            return score;
        }
    }
    return 0;
}

int posibilitieVertical(int board[N][N], int column, int position, int player){
    
    int score = 10;
    int empty = 0;
    int imIn = 0;
    for(int i = 0; i!=N; i++){
        if(board[i][column] == 0){
            empty++;
            score += 10;
        }else if (board[i][column] == player){
            empty++;
            imIn = 1;
            score += 10;
        }else{
            empty = 0;
            imIn = 0;
            score = 10;
        }
        
        if(empty == 4 && imIn == 1){
            return score;
        }
    }
    return 0;
}






int posibilities(int board[N][N], int column, int *scoreV, int *scoreH){
    int position = calculatePosition(column, board);
    position++;
    int player = board[position][column];
    int posibilities = 0;
    int posibilitiesH;
    int posibilitiesV;
    
    if(player == 2){
        posibilitiesH = posibilitieHoritzontal(board, column, position, player);
        posibilitiesV = posibilitieVertical(board, column, position, player);
        posibilities += posibilitiesH;
        posibilities += posibilitiesV;
    }else{
        posibilitiesH = posibilitieHoritzontal(board, column, position, player);
        posibilitiesV = posibilitieVertical(board, column, position, player);
        posibilities += posibilitiesH;
        posibilities += posibilitiesV;
    }
    scoreV = posibilitiesV;
    scoreH = posibilitiesH;
    
    return posibilities;
}


int evaluateMove(int board[N][N], int column, int *scoreV, int *scoreH){
    int score = 0;

    int winner = win(board, column);
    
    if(winner == 2 ){
        return 10000;
    }else if(winner == 1){
        return -10000;
    }
    
    score = posibilities(board, column, &scoreV, &scoreH);
    
    if(score == 0){
        score = rand () % 9;
    }
    
    return score;
}


int evaluateTree(Node *root, int depth, int maxMin){ //Max -> 1 /Min -> 0
    
    int score = evaluateMove(root->board, root->columnUsed, root->scooreVertical, root->scooreHoritzontal);
    
    // If COMPUTER won
    if(score == 10000){
        root->value = 10000;
        return score;
    }
    // If HUMAN won
    if(score == -10000){
        root->value = -10000;
        return score;
    }
    
    //If board is full
    if(boardFull(1,root->board) == 1){
        root->value = 0;
        return 0;
    }
    
    if(root->n_children==0){
        root->value = score;
        return score;
    }
    
    if(maxMin == 1){ //Max
        int best = -100000;
        
        for(int i = 0;  i != root->n_children; i++){
            best = max(best, evaluateTree(root->children[i]->board,depth+1,0));
        }
        root->value = best;
        return best;
        
    }else{ //Min
        int best = 100000;
        
        for(int i = 0;  i != root->n_children; i++){
            best = min(best, evaluateTree(root->children[i]->board,depth+1,1));
        }
        root->value = best;
        return best;
    }
    
    
}




void inicialiceRoot(int board[N][N], Node *root){
    root->columnUsed = 0;
    root->positionUsed = 0;
    root->scooreHoritzontal = 0;
    root->scooreVertical = 0;
    for(int i = 0; i != N; i++){
        for(int j = 0; j != N; j++){
            root->board[i][j] = board[i][j];
        }
    }
}



int knowMove(Node *root, int score, int *scoreV, int *scoreH){

    int childrenNumber = -1;
    int column = -1;
    
    for(int i = 0; i!= root->n_children ;i++){
        //printf("%i \n", root->children[i]->value);
        if(score == root->children[i]->value){
            childrenNumber = i;
        }
    }
    //printf("%i Columna\n", childrenNumber);
    

    for(int i = 0;i!=N;i++){
        for(int j = 0;j!=N; j++){
            if(root->board[i][j] != root->children[childrenNumber]->board[i][j]){
                column = j+1;
                scoreH = root->scooreHoritzontal;
                scoreV = root->scooreVertical;
            }
        }
    }
    
    return column;
}



//END IA




void inicializeBoard(int board[N][N]){
    for (int i = 0; i != N; i++) {
        for (int j = 0; j != N; j++) {
            board[i][j] = 0;
        }
    }
}

void printBoard(int board[N][N]){
    printf("  1   2   3   4   5   6   7   8\n");
    printf("---------------------------------\n");
    for (int i = 0; i != N; i++) {
         printf("| ");
        for (int j = 0; j != N; j++) {
            if(board[i][j] == 2){
                printf("C | ");
            }else if(board[i][j] == 1) {
                printf("H | ");
            }else{
                printf("  | ");
            }
        }
        printf("\n");
        printf("---------------------------------\n");
    }
}

int changeTurn(int turn){
    if ( turn == 0 ) {
        turn = 1;
    } else{
        turn = 0;
    }
    return turn;
}




int inteligence(int column, int board[N][N]){
   
    int random = 0;
    int bestMove = -1000;
    int score;
    int scoreV;
    int scoreH;
    
    //printf("ENTRANDO EN IA\n");
    Node root;
    inicialiceRoot(board, &root);
    root.n_children = numOfChildren(board);
    createTree(&root,1);
    score = evaluateTree(&root,0,1);
    
    column = knowMove(&root, score, &scoreV, &scoreH);
    
   
    
    //printf("La IA ha decidido %i porque ha obtenido %i puntos verticales y %i puntos horizontales.\n", column,scoreV, scoreH );
   
    
    //printf("SALIENDO EN IA\n");
    /*printf("Score de IA: %d \n",score);
    printf("Columna de IA: %d \n",column);
    
    /*if(score > bestMove){
        column = 0;
    } else{
        random = 1;
    }*/
    
    
    int isCorrect = 0;
      do{
          if(random == 1){
              column = rand () % 9;
          }
        if(column > 8 || column < 1){
            isCorrect = 1;
        } else {
            isCorrect = isColumnFull(column-1, board);
        }
        
      }while(isCorrect == 1);
    printf("The computer selects the column %d \n",column);
      return column;
}

int isColumnFull(int column, int board[N][N]){
    if(board[0][column] == 0){
        return 0;
    }
    return 1;
}

int askHuman(int column, int board[N][N]){
    int isCorrect = 0;
    do{
        printf("Please select a column to deposit your file: \n");
        scanf("%d", &column);
        if(column > N || column < 1){
            isCorrect = 1;
            printf("This column doesn't exist. \n");
        } else {
            isCorrect = isColumnFull(column-1, board);
            if(isCorrect == 1){
                printf("This column is full. \n");
            }
        }
        
    }while(isCorrect == 1);
    return column;
}

int calculatePosition(int column, int board[N][N]){
    int isNotFree = 0;
    int position = N-1;
    do{
        if(board[position][column] == 0){
            isNotFree = 1;
        }else{
            position--;
            
        }
    }while(isNotFree == 0 && position > 0);
    return position;
}

void moves(int column, int board[N][N], int turn){
    int position = calculatePosition(column-1, board);
    
    if(turn == 0){
        board[position][column-1] = 1;
    }else{
        board[position][column-1] = 2;
    }
}

int boardIsFull(int board[N][N]){
    for (int i = 0; i != N; i++){
        if(board[0][i] == 0){
            return 0;
        }
    }
    return 1;
}

int winHorizontal(int board[N][N], int column, int position){
    int player = board[position][column];
    int win = 0;
    int consecutive = 0;
    for(int i = 0; i != N; i++){
        if(board[position][i] == player){
            consecutive++;
            if(consecutive == 4){
                win = 1;
            }
        } else{
            consecutive = 0;
        }
    }
    if(win == 1){
        return player;
    }else{
        return win;
    }
}

int winVertical(int board[N][N], int column, int position){
    int player = board[position][column];
    int win = 0;
    int consecutive = 0;
    for(int i = 0; i != N; i++){
        if(board[i][column] == player){
            consecutive++;
            if(consecutive == 4){
                win = 1;
            }
        } else{
            consecutive = 0;
        }
    }
    if(win == 1){
        return player;
    }else{
        return win;
    }
}

int winDiagonal(int board[N][N], int column, int position){
   
    int player = board[position][column];
    int pauseLoop = 0;
    int win = 0;
    int consecutive = 0;
    
    if(position != 0 && column != 0){
        
        do{
            if(position != 0 && column != 0){
                position--;
                column--;
            }else{
                pauseLoop = 1;
            }
        }while(pauseLoop == 0);
    }
   
    
    do{
        if(board[position][column] == player){
            consecutive++;
            if(consecutive == 4){
                win = 1;
            }
        } else{
            consecutive = 0;
        }
        position++;
        column++;
        
    }while(position != N && column != N);
    
    
    if(win == 1){
        return player;
    }else{
        return win;
    }
}

int winDiagonalTwo(int board[N][N], int column, int position){
   
    int player = board[position][column];
    int pauseLoop = 0;
    int win = 0;
    int consecutive = 0;
    
    if(position != 0 && column != N){
        
        do{
            if(position != 0 && column != N){
                position--;
                column++;
            }else{
                pauseLoop = 1;
            }
        }while(pauseLoop == 0);
    }
   
    
    do{
        if(board[position][column] == player){
            consecutive++;
            if(consecutive == 4){
                win = 1;
            }
        } else{
            consecutive = 0;
        }
        position++;
        column--;
        
    }while(position != N && column != -1);
    
    
    if(win == 1){
        return player;
    }else{
        return win;
    }
}



int win(int board[N][N], int column){
    
    int position = calculatePosition(column, board);
    position++;
    /*printf("POSICIONES\n");
    printf("COLUMNA %i \n", column+1);
    printf("FILA %i \n", position+1);*/
    int win = 0;
    //horizontal
    win = winHorizontal(board, column, position);
    
    //vertical
    if(win == 0){
        win = winVertical(board, column, position);
    }
    //diagonal
    
    if(win == 0){
        win = winDiagonal(board, column, position);
    }
    
    if(win == 0){
        win = winDiagonalTwo(board, column, position);
    }
    
    return win;
}

int boardFull(int finally, int board[N][N]){
    finally = boardIsFull(board);  // 0-> Not full, 1-> IsFull
    return finally;
}


int main(int argc, const char * argv[]) {
    
    //srand(time(NULL));
    
    int board[N][N]; // 0-> nothing 1-> human 2->computer
    int finallyFull = 0; // 0->continue 1->full
    int turn = 0; // 0->human 1->computer
    int column;
    int finallyWin = 0; // 1->human wins 2->computer wins 0-> continue
    
    inicializeBoard(board);
    
    do{
        printBoard(board);
        column = askHuman(column, board);
        moves(column, board, turn);
        finallyWin = win(board, column-1);
        turn = changeTurn(turn);
        printBoard(board);
        if (finallyWin == 0){
        column = inteligence(column, board);
        moves(column, board, turn);
        finallyWin = win(board, column-1);
        }
        turn = changeTurn(turn);

        finallyFull = boardFull(finallyFull, board);
    }while(finallyFull == 0 && finallyWin == 0);
    printBoard(board);
    
    if(finallyFull == 1){
        printf("The board is full. \n");
    }
    if(finallyWin == 2){
        printf("Computer wins. \n");
    }else if (finallyWin == 1){
        printf("Human wins. \n");
    }
    
    return 0;
}
